var express = require('express');
var router  = express.Router();

router.get('/', function(req, res) {
    res.render('index', {  title: 'Main page' });
});
router.get('/profile', function(req, res) {
    res.render('profile', { title: 'Profile' });
});
router.get('/docs', function(req, res) {
    res.render('docs', { title: 'Docs' });
});
router.get('/notifications', function(req, res) {
    res.render('notifications', { title: 'Notifications' });
});
router.get('/login', function(req, res) {
    res.render('login', { title: 'Login' });
});

module.exports = router;
